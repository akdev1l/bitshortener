/*
 * Author: ealejandro
 */

#include "applicationui.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>
#include <bb/cascades/Page>
#include <bb/cascades/Container>
#include <bb/cascades/SceneCover>
#include "BitLyManager.hpp"
#include "RegistrationHandler.hpp"

using namespace bb::cascades;
using namespace bb::system;

ApplicationUI::ApplicationUI(bb::cascades::Application *app) :
        QObject(app)
{
    // prepare the localization
    m_pTranslator = new QTranslator(this);
    m_pLocaleHandler = new LocaleHandler(this);
    if(!QObject::connect(m_pLocaleHandler, SIGNAL(systemLanguageChanged()), this, SLOT(onSystemLanguageChanged()))) {
        // This is an abnormal situation! Something went wrong!
        // Add own code to recover here
        qWarning() << "Recovering from a failed connect()";
    }
    // initial load
    onSystemLanguageChanged();

    m_BitlyManager = new BitlyManager();
    // Create scene document from main.qml asset, the parent is set
    // to ensure the document gets destroyed properly at shut down.
    QmlDocument *qml;
    QmlDocument *expandQml;
    QmlDocument *activeFrame;
    Container *activeRoot;
    AbstractPane *root;
    if(m_InvokeManager.startupMode() == ApplicationStartupMode::LaunchApplication){
    	m_IsCard = false;
    	qml = QmlDocument::create("asset:///main.qml").parent(this);
    	expandQml = QmlDocument::create("asset:///LinkExpander.qml").parent(this);
    	expandQml->setContextProperty("app", this);
		activeFrame = QmlDocument::create("asset:///ActiveFrame.qml").parent(this);
    	const QUuid uuid(QLatin1String("d83f0ae9-96e0-4e4f-8304-c075c979fea7"));
    	registrationHandler = new RegistrationHandler(uuid, this);
		registrationHandler->registerApplication();
		activeRoot = activeFrame->createRootObject<Container>();
		root = qml->createRootObject<AbstractPane>();
		SceneCover *cover = SceneCover::create().content(activeRoot);
		Application::instance()->setCover(cover);
		m_ExpandButton = root->findChild<Button*>("expandButton");
		m_ShortUrl = root->findChild<TextField*>("shortUrl");
		m_ShortUrlResultQrCode = root->findChild<ImageView*>("expandUrlResultQrCode");
		m_ExpandResultUrl = root->findChild<Label*>("expandResultUrl");
		m_ActiveImage = activeRoot->findChild<ImageView*>("activeImage");
		m_ActiveLabel = activeRoot->findChild<Label*>("activeLabel");
		QObject::connect(m_ExpandButton, SIGNAL(clicked()), SLOT(sendExpandRequest()));
    }
    else if(m_InvokeManager.startupMode() == ApplicationStartupMode::InvokeCard){
    	m_IsCard = true;
    	m_CardCounter = 0;
    	qml = QmlDocument::create("asset:///card/main.qml").parent(this);
    	root = qml->createRootObject<AbstractPane>();
    	QObject::connect(&m_InvokeManager, SIGNAL(invoked(const bb::system::InvokeRequest&)), this, SLOT(onInvoked(const bb::system::InvokeRequest&)));
    	QObject::connect(&m_InvokeManager, SIGNAL(childCardDone(const bb::system::CardDoneMessage&)), SLOT(onChildCardDone(const bb::system::CardDoneMessage&)));
    }

	qml->setContextProperty("app", this);




    m_ImageGetter = new QNetworkAccessManager();
    m_ShrinkButton = root->findChild<Button*>("shrinkButton");
    m_LongUrl = root->findChild<TextField*>("longUrl");
    m_LongUrlResultQrCode = root->findChild<ImageView*>("shrinkUrlQrCode");
    m_ShrinkResultUrl = root->findChild<Label*>("shrinkResultUrl");



    QObject::connect(m_ShrinkButton, SIGNAL(clicked()), SLOT(sendRequest()));
    QObject::connect(m_BitlyManager, SIGNAL(requestFinished(QUrl, QVariant)), SLOT(changeText(QUrl)));
    QObject::connect(m_ImageGetter, SIGNAL(finished(QNetworkReply*)), SLOT(changeImage(QNetworkReply*)));


    // Set created root object as the application scene
    app->setScene(root);
}

void ApplicationUI::onSystemLanguageChanged()
{
    QCoreApplication::instance()->removeTranslator(m_pTranslator);
    // Initiate, load and install the application translation files.
    QString locale_string = QLocale().name();
    QString file_name = QString("Bit_lyClient_%1").arg(locale_string);
    if (m_pTranslator->load(file_name, "app/native/qm")) {
        QCoreApplication::instance()->installTranslator(m_pTranslator);
    }
}
void ApplicationUI::sendRequest(){
	m_BitlyManager->sendRequest(QUrl(m_LongUrl->text()), BitlyManager::Shrink);
	m_Shrinking = true;
}
void ApplicationUI::changeText(QUrl p_Url){
	if(m_Shrinking){
		m_ShrinkResultUrl->setText(p_Url.toString());
		if(!m_IsCard)
			m_ActiveLabel->setText(m_LongUrl->text());
		if(p_Url.isValid()){
			m_ImageGetter->get(QNetworkRequest(QUrl(p_Url.toString()+".qrcode")));
		}
	}
	else{
		m_ExpandResultUrl->setText(p_Url.toString());
		m_ImageGetter->get(QNetworkRequest(QUrl(m_ShortUrl->text()+".qrcode")));
	}
}
void ApplicationUI::changeImage(QNetworkReply* p_Image){
	Image qrCode = p_Image->readAll();
	if(m_Shrinking){
		m_LongUrlResultQrCode->setImage(qrCode);
		if(!m_IsCard)
			m_ActiveImage->setImage(qrCode);
	}
	else{
		m_ShortUrlResultQrCode->setImage(qrCode);
	}
}
void ApplicationUI::sendExpandRequest(){
	m_BitlyManager->sendRequest(QUrl(m_ShortUrl->text()), BitlyManager::Expand);
	m_Shrinking = false;
}
void ApplicationUI::CopyToClipBoard(QString p_Url){
	m_ClipBoard.insert("text/plain", p_Url.toAscii());
}
void ApplicationUI::onInvoked(const InvokeRequest& p_Request){
	m_LongUrl->setText(p_Request.uri().toString());
	qDebug() << "ACTION = " << p_Request.action() << "\n" << p_Request.uri();
}
void ApplicationUI::closeCard(QString p_Reason){
	qDebug() << p_Reason;
	CardDoneMessage message;
	QString reason = p_Reason;
	message.setReason(reason);
	m_InvokeManager.sendCardDone(message);
}
void ApplicationUI::onChildCardDone(const bb::system::CardDoneMessage &p_Message){
	m_CardCounter++;
	qDebug() << m_CardCounter;
	if(m_CardCounter%2==0){
		closeCard(p_Message.reason());
	}
}
