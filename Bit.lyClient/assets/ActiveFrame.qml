import bb.cascades 1.0

Container {
    background: Color.White
    property alias imageSrc : imageQr.image
    property alias urlText : linkText.text
    ImageView{
        objectName: "activeImage"
        id: imageQr
        preferredWidth: 334.0
        preferredHeight: 356.0
        minWidth: 310.0
        minHeight: 211.0
        imageSource: "asset:///images/19Z6pTK.png"
    }
    Container {
        bottomPadding: 12.0
        Label {
            objectName: "activeLabel"
            id: linkText
            textStyle.fontSize:FontSize.XLarge
            textStyle.fontStyle: FontStyle.Italic
            text: "rpfather.com"
        }
    }
}
