import bb.cascades 1.0
import bb.system 1.0
Page {
    attachedObjects: [
        Invocation {
            id: invokateBrowser
            query{
                mimeType: "text/html"
                invokeActionId: "bb.action.OPEN"
                onQueryChanged: {
                    query.updateQuery();
                }
            }
            onArmed: {
                trigger("bb.action.OPEN");
            }
        },
        SystemToast {
            id: copiedToast2
            body: "Copied link to clipboard"
        }
    ]
    actions: [
        ActionItem {
            title: "Copy to clipboard"
            imageSource: "asset:///images/icons/copy-icon-th.png"
            onTriggered: {
                app.CopyToClipBoard(expandedUrlLabel.text)
                copiedToast2.show()
            }
        },
        InvokeActionItem {
            title: "Share link!"
            query{
                mimeType: "text/plain"
                invokeActionId: "bb.action.SHARE"
            }
            onTriggered: {
                data = expandedUrlLabel.text
            }
            ActionBar.placement: ActionBarPlacement.OnBar
        }
    ]
    titleBar: TitleBar {
        title: "Bit.ly Expander"
        kind: TitleBarKind.Default
        appearance: TitleBarAppearance.Default
    }
    Container{
        Container {
            topMargin: 0.0
            topPadding: 20.0
            leftPadding: 12.0
            rightPadding: 12.0
            bottomMargin: 20.0
            Label {
                text: qsTr("Insert Bit.ly url:") + Retranslate.onLocaleOrLanguageChanged
            }
            TextField {
                hintText: "http://bit.ly/19Z6pTK"
                objectName: "shortUrl"
                inputMode: TextFieldInputMode.Url
                onFocusedChanged: {
                    if(focused && text==""){
                        text = "http://"
                    }
                }
            }
            Button {
                text: qsTr("Expand Url!")
                horizontalAlignment: HorizontalAlignment.Center
                objectName: "expandButton"
            }
        }
        Container {
            layout: StackLayout {
            
            }
            layoutProperties: StackLayoutProperties {
                spaceQuota: 4.0
            }
            preferredWidth: 768.0
            Container {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1.0
                
                }
                leftPadding: 20.0
                preferredWidth: 768.0
                maxWidth: 720.0
                Label {
                    text: "http://www.rpfather.com"
                    //textStyle.color: Color.create("#61b3de")
                    textStyle.fontStyle: FontStyle.Italic
                    textStyle.fontSize: FontSize.PercentageValue
                    textStyle.fontSizeValue: 150.00
                    textStyle.textAlign: TextAlign.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center
                    objectName: "expandResultUrl"
                    id: expandedUrlLabel
                    contextActions: [
                        ActionSet {
                            actions: ActionItem {
                                title: "Copy to clipboard"
                                imageSource: "asset:///images/icons/copy-icon-th.png"
                                onTriggered: {
                                    app.CopyToClipBoard(expandedUrlLabel.text)
                                    copiedToast2.show()
                                }
                            }
                        }
                    ]
                }
            }
            Container {
                horizontalAlignment: HorizontalAlignment.Center
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 6.0

                }
                ImageView {
                    imageSource: "asset:///images/19Z6pTK.png"
                    horizontalAlignment: HorizontalAlignment.Center
                    layoutProperties: StackLayoutProperties {
                    
                    }
                    preferredWidth: 500.0
                    preferredHeight: 500.0
                    objectName: "expandUrlResultQrCode"
                    onTouch: {
                        invokateBrowser.query.uri = expandedUrlLabel.text
                    }
                }
            }
        }
    }
}
