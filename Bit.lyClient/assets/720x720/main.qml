import bb.cascades 1.0
import bb.system 1.0

TabbedPane {
    attachedObjects: [
        Invocation {
            id: invokateBrowser
            query{
                mimeType: "text/html"
                invokeActionId: "bb.action.OPEN"
                onQueryChanged: {
                    query.updateQuery()
                }
            }
            onArmed: {
                trigger("bb.action.OPEN")
            }
        },
        SystemToast {
            id: copiedToast
            body: "Copied link to clipboard"
        }
    ]
    Menu.definition: [
        MenuDefinition{
            actions: [
                ActionItem {
                    imageSource: "asset:///images/icons/ic_info.png"
                    title: "Contact Us!"
                    attachedObjects: [
                        Invocation {
                            id: invokateContactUs
                            query.mimeType: ""
                            query.uri: "mailto:ealejandro@rpfather.com?subject=BitShortener%20:%20Support"
                            query.invokeTargetId: "sys.pim.uib.email.hybridcomposer"
                        }
                    ]
                    onTriggered: {
                        invokateContactUs.trigger("bb.action.SENDEMAIL")
                    }
                }
            ]
        }
    ]
    Tab { //First tab
        // Localized text with the dynamic translation and locale updates support
        imageSource: "asset:///images/icons/shrinkIcon.png"
        title: qsTr("Shrink URL") + Retranslate.onLocaleOrLanguageChanged
        Page {
            actions: [
                ActionItem {
                    title: "Copy to clipboard"
                    imageSource: "asset:///images/icons/copy-icon-th.png"
                    onTriggered: {
                        app.CopyToClipBoard(resultShrinkUrl.text)
                        copiedToast.show()
                    }
                },
                InvokeActionItem {
                    title: "Share link!"
                    query{
                        mimeType: "text/plain"
                        invokeActionId: "bb.action.SHARE"
                    }
                    onTriggered: {
                        data = resultShrinkUrl.text
                    }
                    ActionBar.placement: ActionBarPlacement.OnBar
                }
            ]
            titleBar: TitleBar {
                title: "URL Shortener"
                kind: TitleBarKind.Default
                appearance: TitleBarAppearance.Default
            }
            Container{
                Container {
                    topMargin: 0.0
                    topPadding: 20.0
                    leftPadding: 12.0
                    rightPadding: 12.0
                    bottomMargin: 20.0
                    Label {
                        text: qsTr("Insert long url:") + Retranslate.onLocaleOrLanguageChanged
                    }
                    TextField {
                        objectName: "longUrl"
                        hintText: "http://www.rpfather.com"
                        inputMode: TextFieldInputMode.Url
                        onFocusedChanged: {
                            if(focused && text==""){
                                text = "http://"
                            }
                        }
                    }
                    Button {
                        objectName: "shrinkButton"
                        text: qsTr("Shrink Url!")
                        horizontalAlignment: HorizontalAlignment.Center
                    }
                }
                Container {
                    layout: StackLayout {

                    }
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 4.0
                    }
                    preferredWidth: 768.0
                    Container {
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 1.0

                        }
                        leftPadding: 20.0
                        preferredWidth: 768.0
                        maxWidth: 720.0
                        Label {
                            text: "http://bit.ly/19Z6pTK"
                            //textStyle.color: Color.create("#61b3de")
                            textStyle.fontStyle: FontStyle.Italic
                            textStyle.fontSize: FontSize.PercentageValue
                            textStyle.fontSizeValue: 200.00
                            textStyle.textAlign: TextAlign.Center
                            horizontalAlignment: HorizontalAlignment.Center
                            verticalAlignment: VerticalAlignment.Center
                            objectName: "shrinkResultUrl"
                            id: resultShrinkUrl
                            contextActions: [
                                ActionSet {
                                    actions: ActionItem {
                                        title: "Copy to clipboard"
                                        imageSource: "asset:///images/icons/copy-icon-th.png"
                                        onTriggered: {
                                            app.CopyToClipBoard(resultShrinkUrl.text)
                                            copiedToast.show()
                                        }
                                    }
                                }
                            ]
                        }
                    }
                    Container {
                        horizontalAlignment: HorizontalAlignment.Center
                        layoutProperties: StackLayoutProperties {
                            spaceQuota: 4

                        }
                        ImageView {
                            id: shrinkUrlQrCode
                            visible: false
                            imageSource: "asset:///images/19Z6pTK.png"
                            horizontalAlignment: HorizontalAlignment.Center
                            layoutProperties: StackLayoutProperties {
    
                            }
                            preferredWidth: 500.0
                            preferredHeight: 500.0
                            objectName: "shrinkUrlQrCode"
                            onTouch: {
                                invokateBrowser.query.uri = resultShrinkUrl.text
                            }
                        }
                    }
                }
            }
        }
    } //End of first tab
    Tab { //Second tab
        id: tab2
        imageSource: "asset:///images/icons/expandIcon.png"
        title: qsTr("Bit.ly Expander") + Retranslate.onLocaleOrLanguageChanged
        content: LinkExpander {
            
        }
    } //End of second tab
}
