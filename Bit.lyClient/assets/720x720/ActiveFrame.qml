import bb.cascades 1.0

Container {
    background: Color.Black
    property alias imageSrc : imageQr.image
    property alias urlText : linkText.text
    ImageView{
        objectName: "activeImage"
        id: imageQr
        preferredWidth: 290.0
        preferredHeight: 200.0
        minWidth: 290.0
        minHeight: 211.0
        imageSource: "asset:///images/19Z6pTK.png"
        scalingMethod: ScalingMethod.AspectFit
    }
    Container {
        background: Color.Black
        bottomPadding: 20.0
        Label {
            objectName: "activeLabel"
            id: linkText
            textStyle.fontSize:FontSize.XLarge
            textStyle.fontStyle: FontStyle.Italic
            textStyle.color: Color.White
            text: "rpfather.com"
        }
    }
}
