import bb.cascades 1.0
import bb.system 1.0

Page {
    attachedObjects: [
        Invocation {
            id: invokeShare
            query{
                invokeActionId: "bb.action.SHARE"
                mimeType: "text/plain"
                onQueryChanged: {
                    query.updateQuery();
                }
            }
            onArmed: {
                console.log("QUERYDATA-------"+query.data);
                if(query.data!=""){
                    trigger("bb.action.SHARE");
                }
            }
        },
        ActionItem {
            id: close_card
            title: "Cancel"
            objectName: "close_card"
            onTriggered: {
                app.closeCard("Closed by user"); 
            }
        }
    ]
    titleBar: TitleBar {
        title: "URL Shortener"
        kind: TitleBarKind.Default
        appearance: TitleBarAppearance.Default
        dismissAction: close_card
    }
    Container{
        Container {
            topMargin: 0.0
            topPadding: 20.0
            leftPadding: 12.0
            rightPadding: 12.0
            bottomMargin: 20.0
            Label {
                text: qsTr("Insert long url:") + Retranslate.onLocaleOrLanguageChanged
            }
            TextField {
                objectName: "longUrl"
                hintText: "http://www.rpfapps.com"
                inputMode: TextFieldInputMode.Url
                onFocusedChanged: {
                    if(focused && text==""){
                        text = "http://"
                    }
                }
            }
            Button {
                objectName: "shrinkButton"
                text: qsTr("Shrink Url!")
                horizontalAlignment: HorizontalAlignment.Center
                onClicked: {
                    if(invokeShare.query.data == resultShrinkUrl.text)
                    invokeShare.armed();
                }
            }
        }
        Container {
            layout: StackLayout {

            }
            layoutProperties: StackLayoutProperties {
                spaceQuota: 4.0
            }
            preferredWidth: 768.0
            Container {
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 1.0

                }
                leftPadding: 20.0
                preferredWidth: 768.0
                maxWidth: 720.0
                Label {
                    text: "http://bit.ly/19Z6pTK"
                    textStyle.fontStyle: FontStyle.Italic
                    textStyle.fontSize: FontSize.PercentageValue
                    textStyle.fontSizeValue: 200.00
                    textStyle.textAlign: TextAlign.Center
                    horizontalAlignment: HorizontalAlignment.Center
                    verticalAlignment: VerticalAlignment.Center
                    objectName: "shrinkResultUrl"
                    id: resultShrinkUrl
                }
            }
            Container {
                horizontalAlignment: HorizontalAlignment.Center
                layoutProperties: StackLayoutProperties {
                    spaceQuota: 4

                }
                ImageView {
                    id: shrinkUrlQrCode
                    imageSource: "asset:///images/19Z6pTK.png"
                    horizontalAlignment: HorizontalAlignment.Center
                    layoutProperties: StackLayoutProperties {

                    }
                    preferredWidth: 500.0
                    preferredHeight: 500.0
                    objectName: "shrinkUrlQrCode"
                    onImageSourceChanged: {
                        
                        invokeShare.query.data = resultShrinkUrl.text;
                        
                        console.log("IMAGESOURCECHANGED------"+invokeShare.query.data);
                    }
                }
            }
        }
    }
}
