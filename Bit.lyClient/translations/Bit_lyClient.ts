<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>LinkExpander</name>
    <message>
        <location filename="../assets/720x720/LinkExpander.qml" line="57"/>
        <location filename="../assets/LinkExpander.qml" line="57"/>
        <source>Insert Bit.ly url:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/LinkExpander.qml" line="70"/>
        <location filename="../assets/LinkExpander.qml" line="70"/>
        <source>Expand Url!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationHandler</name>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="37"/>
        <source>Please wait while the application connects to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="141"/>
        <source>Application connected to BBM.  Press Continue.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="153"/>
        <source>Disconnected by RIM. RIM is preventing this application from connecting to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="159"/>
        <source>Disconnected. Go to Settings -&gt; Security and Privacy -&gt; Application Permissions and connect this application to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="167"/>
        <source>Invalid UUID. Report this error to the vendor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="173"/>
        <source>Too many applications are connected to BBM. Uninstall one or more applications and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="181"/>
        <source>Cannot connect to BBM. Download this application from AppWorld to keep using it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="187"/>
        <source>Check your Internet connection and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="194"/>
        <source>Connecting to BBM. Please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="199"/>
        <source>Determining the status. Please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/RegistrationHandler.cpp" line="209"/>
        <source>Would you like to connect the application to BBM?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/720x720/main.qml" line="48"/>
        <location filename="../assets/main.qml" line="51"/>
        <source>Shrink URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/main.qml" line="84"/>
        <location filename="../assets/card/main.qml" line="45"/>
        <location filename="../assets/main.qml" line="87"/>
        <source>Insert long url:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/main.qml" line="98"/>
        <location filename="../assets/card/main.qml" line="59"/>
        <location filename="../assets/main.qml" line="104"/>
        <source>Shrink Url!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/main.qml" line="172"/>
        <location filename="../assets/main.qml" line="180"/>
        <source>Bit.ly Expander</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
