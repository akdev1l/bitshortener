/*
 * BitLyManager.cpp
 *
 *  Created on: Aug 28, 2013
 *      Author: ealejandro
 */
#include <BitLyManager.hpp>

BitlyManager::BitlyManager(){
	m_BaseUrl = new QUrl("https://api-ssl.bitly.com/v3/");
	m_NetworkManager = new QNetworkAccessManager();
	m_JsonHandler = new bb::data::JsonDataAccess();
	m_RequestType = noRequest;
	m_AccessToken = QString("&access_token=61d486f42fbd6cd3e54b9aaa2dae8eb98159209a");
}
void BitlyManager::sendRequest(QUrl p_Url, RequestType p_Type){
	QUrl requestUrl;
	qDebug() << p_Url;
	m_RequestType = p_Type;
	if(p_Type == Shrink){
		requestUrl = QUrl(m_BaseUrl->toString() + "shorten?domain=bit.ly&format=json&longUrl="+p_Url.toString()+m_AccessToken);
	}
	else{
		requestUrl = QUrl(m_BaseUrl->toString() + "expand?format=json&shortUrl=" +p_Url.toString()+m_AccessToken);
	}
	QNetworkRequest myRequest = QNetworkRequest(requestUrl);
	m_NetworkManager->get(myRequest);
	QObject::connect(m_NetworkManager, SIGNAL(finished(QNetworkReply*)), SLOT(proccessReply(QNetworkReply*)));
}
void BitlyManager::proccessReply(QNetworkReply* p_Reply){
	QVariant jsonReply = m_JsonHandler->loadFromBuffer(p_Reply->readAll());
	QUrl replyUrl;
	if(m_RequestType== Shrink){
		replyUrl = QUrl(jsonReply.toMap()["data"].toMap()["url"].toString());
	}
	else if(m_RequestType == Expand){
		replyUrl = QUrl(jsonReply.toMap()["data"].toMap()["expand"].toList().first().toMap()["long_url"].toString());
	}
	if(!replyUrl.isEmpty()){
		emit requestFinished(replyUrl, jsonReply);
	}
	qDebug() << replyUrl;
	m_RequestType = noRequest;
}
