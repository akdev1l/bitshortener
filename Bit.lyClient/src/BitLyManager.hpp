/*
 * BitLyManager.hpp
 *
 *  Created on: Aug 28, 2013
 *      Author: ealejandro
 */

#ifndef BITLYMANAGER_HPP_
#define BITLYMANAGER_HPP_

#include <QObject>
#include <QUrl>
#include <QVariant>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <bb/data/JsonDataAccess>

class BitlyManager : public QObject{
	Q_OBJECT;
public:
	enum RequestType{
		Shrink, Expand, noRequest
	};
	BitlyManager();
	void sendRequest(QUrl p_Url, RequestType p_Type);
	signals:
		void requestFinished(QUrl p_ResultUrl, QVariant p_Extrainfo);
private slots:
	void proccessReply(QNetworkReply* p_Reply);

private:
	QString m_AccessToken;
	RequestType m_RequestType;
	bb::data::JsonDataAccess *m_JsonHandler;
	QUrl *m_BaseUrl;
	QNetworkAccessManager *m_NetworkManager;
};


#endif /* BITLYMANAGER_HPP_ */
