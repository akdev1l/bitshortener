/*
 * Author: ealejandro
 */
#ifndef ApplicationUI_HPP_
#define ApplicationUI_HPP_

#include <QObject>
#include "BitLyManager.hpp"
#include <bb/cascades/Button>
#include <bb/cascades/TextField>
#include <bb/cascades/ImageView>
#include <bb/cascades/Label>
#include <bb/cascades/ActionItem>
#include <bb/system/ClipBoard>
#include "RegistrationHandler.hpp"
#include <bb/system/InvokeManager>
#include <bb/system/ApplicationStartupMode>
#include <bb/system/InvokeRequest>
#include <bb/system/CardDoneMessage>

using namespace bb::cascades;
using namespace bb::system;

namespace bb
{
    namespace cascades
    {
        class Application;
        class LocaleHandler;
    }
}

class QTranslator;

/*!
 * @brief Application object
 *
 *
 */

class ApplicationUI : public QObject
{
    Q_OBJECT
public:
    ApplicationUI(bb::cascades::Application *app);
    virtual ~ApplicationUI(){ }
    BitlyManager *m_BitlyManager;
    Button *m_ShrinkButton;
    Button *m_ExpandButton;
    TextField *m_LongUrl;
    TextField *m_ShortUrl;
    ImageView *m_ShortUrlResultQrCode;
    ImageView *m_LongUrlResultQrCode;
    Label *m_ShrinkResultUrl;
    Label *m_ExpandResultUrl;
    RegistrationHandler *registrationHandler;
    ImageView *m_ActiveImage;
    Label *m_ActiveLabel;
    QNetworkAccessManager *m_ImageGetter;
    bb::system::Clipboard m_ClipBoard;
    Q_INVOKABLE void CopyToClipBoard(QString p_Url);
    bool m_Shrinking;
    bb::system::InvokeManager m_InvokeManager;
    ActionItem *m_CloseCard;
    bool m_IsCard;
    int m_CardCounter;
public slots:
    void sendRequest();
    void sendExpandRequest();
    void changeText(QUrl p_Url);
    void changeImage(QNetworkReply* p_Image);
    void onInvoked(const bb::system::InvokeRequest& p_Request);
    Q_INVOKABLE void closeCard( QString p_Reason);
    void onChildCardDone(const bb::system::CardDoneMessage &p_Message);
private slots:
    void onSystemLanguageChanged();
private:
    QTranslator* m_pTranslator;
    bb::cascades::LocaleHandler* m_pLocaleHandler;
};

#endif /* ApplicationUI_HPP_ */
